<?php
namespace SWApp\Database;

//class for quickly generating an SQL query string
class DBQuery
{
	protected string $query;
	
	public function select(string $table, string $values) : DBQuery
	{
		$this->query = "SELECT {$values} FROM {$table}";
		
		return $this;
	}
	
	public function leftJoin(string $columns, string $on) : DBQuery
	{
		$this->query .= " LEFT JOIN {$columns} ON {$on}";
		
		return $this;
	}
	
	public function orderBy(string $value, bool $asc = true) :  DBQuery
	{
		$order_by = $value != '' ? "ORDER BY {$value} " . ($asc ? 'ASC' : 'DESC') : "";
		$this->query .= " {$order_by}";
		
		return $this;
	}
	
	public function limit(int $limit) : DBQuery
	{
		if ($limit == 0)
			return $this;
		
		$this->query .= " LIMIT {$limit}";
		
		return $this;
	}
	
	public function getString() : string
	{
		return $this->query;
	}
}

?>