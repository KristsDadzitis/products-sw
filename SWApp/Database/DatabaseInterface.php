<?php
namespace SWApp\Database;

use SWApp\Database\DBQuery;

interface DatabaseInterface
{
	public function connect();
	public function getConnection();
	public function query(string $query);
	public function queryWithFetch(string $query);
	public function fetch(string $table, string $query, int $limit, string $orderBy, bool $asc);
	public function fetchDBQuery(DBQuery $query);
	public function insert(string $table, array $vars);
	public function insertJSON(string $table, array $data);	
	public function delete(string $table, string $what, string $values);
}

?>