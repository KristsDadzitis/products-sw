<?php
namespace SWApp\Database;

use SWApp\Database\DatabaseInterface;

class MySqlDB implements DatabaseInterface
{
	protected $connection = null;
	
	//database config - edit as required
	protected string $host = '127.0.0.1';
	protected string $user = 'root';
	protected string $password = '';
	protected string $database = 'products_sw';
	//end database config
	
	public function connect()
	{
		$mysqli = new \mysqli($this->host, $this->user, $this->password, $this->database);
		
		$this->connection = $mysqli;
		
		if ($mysqli->connect_errno)
		{
			echo "<br>Failed to connect to MySQL: " . $mysqli->connect_error . "<br><br>";			
			
			return;
		}
		
		$mysqli->set_charset('utf8');
	}
	
	public function getConnection(): \mysqli
	{
		return $this->connection;
	}
	
	//basic query with no results returned
	public function query(string $query): bool
	{
		if ($this->connection->connect_errno)
			return [];
		
		$res = $this->connection->query($query);
		
		if (!$this->connection->query($query))
		{
			echo "<br> SQL ERROR: (" . $this->connection->errno . ") " . $this->connection->error . "<br><br>";
			return false;
		}
		
		return true;
	}
	
	//basic query with results
	public function queryWithFetch(string $query): array
	{
		if ($this->connection->connect_errno)
			return [];
		
		$res = $this->connection->query($query);
		
		$rows = [];
		
		for ($i = 0; $i < $res->num_rows; $i++)
			$rows[] = $res->fetch_assoc();
		
		return $rows;
	}
	
	//fetch simple queries
	public function fetch(string $table, string $query = '*', int $limit = 0, string $orderBy = '', bool $asc = true): array
	{		
		if ($this->connection->connect_errno)
			return [];
		
		$add_limit = $limit > 0 ? "LIMIT {$limit}" : "";
		$order_by = $orderBy != '' ? "ORDER BY {$orderBy} " . ($asc === true ? 'ASC' : 'DESC') : "";

		$query = "SELECT {$query} FROM {$table}  {$order_by} {$add_limit}";
		
		$res = $this->connection->query($query);
		
		$rows = [];
		
		for ($i = 0; $i < $res->num_rows; $i++)
			$rows[] = $res->fetch_assoc();
		
		return $rows;
	}
	
	//fetch data with DBQuery class as input
	public function fetchDBQuery(DBQuery $query): array
	{
		if ($this->connection->connect_errno)
			return [];
		
		$res = $this->connection->query($query->getString());
		
		if ($this->connection->errno)
		{
			echo "<br> SQL ERROR: (" . $this->connection->errno . ") " . $this->connection->error . "<br><br>";
			
			return [];
		}
		
		$rows = [];
		
		for ($i = 0; $i < $res->num_rows; $i++)
			$rows[] = $res->fetch_assoc();
		
		return $rows;
	}
	
	public function insert(string $table, array $vars): bool
	{
		if ($this->connection->connect_errno)
			return false;

		$keys = implode(", ", array_keys($vars));
		$values = "'" . implode("', '", $vars) . "'";
		
		$query = "INSERT INTO {$table}({$keys}) VALUES({$values})";
		
		if (!$this->connection->query($query))
		{
			echo "<br> SQL ERROR: (" . $this->connection->errno . ") " . $this->connection->error . "<br><br>";
			return false;
		}
		
		return true;
	}
	
	public function insertJSON(string $table, array $data): bool
	{
		if ($this->connection->connect_errno)
			return false;			

		$json = json_encode($data);
		$query = "INSERT INTO {$table}(Data) VALUES('{$json}')";
		
		if (!$this->connection->query($query))
		{
			echo "<br> SQL ERROR: (" . $this->connection->errno . ") " . $this->connection->error . "<br><br>";
			return false;
		}
		
		return true;
	}
	
	//delete query that accepts comma seperated values for the mass delete function
	public function delete(string $table, string $what, string $values): bool
	{
		if ($this->connection->connect_errno)
			return false;
		
		$query = "DELETE FROM {$table} WHERE {$what} IN ({$values})"; //$values does not work with strings at the moment because no quotes are added
		
		$this->connection->query($query);
		
		return true;
	}
}
	
?>
