<?php
namespace SWApp;

use SWApp\Database\DatabaseInterface;
use SWApp\Models\Book;
use SWApp\Models\DVD;
use SWApp\Models\Furniture;

class Page
{
	public function goToPage(string $url)
	{
		header("Location: {$url}.php");
		exit;
	}
	
	public function createSampleData(DatabaseInterface $db)
	{		
		//products table
		$db->query("DROP TABLE IF EXISTS products");
		$db->query("CREATE TABLE products(
							id INT NOT NULL AUTO_INCREMENT,
							sku VARCHAR(255) NOT NULL,
							name VARCHAR(255) NOT NULL,
							price FLOAT NOT NULL,
							type int DEFAULT 1,
							data JSON NULL,
							PRIMARY KEY (id)
						);"
					);
		
		//types table
		$db->query("DROP TABLE IF EXISTS types");
		$db->query("CREATE TABLE types(
								type_id INT NOT NULL AUTO_INCREMENT,
								type_name VARCHAR(128) NOT NULL,
								type_display_name VARCHAR(128) NOT NULL,
								PRIMARY KEY (type_id)
							);"
					);
		
		//populate types table
		$db->insert('types', ['type_name' => 'product', 'type_display_name' => 'Other']);
		$db->insert('types', ['type_name' => 'book', 'type_display_name' => 'Book']);
		$db->insert('types', ['type_name' => 'dvd', 'type_display_name' => 'DVD']);
		$db->insert('types', ['type_name' => 'furniture', 'type_display_name' => 'Furniture']);
		
		//populate products table
		for ($i = 0; $i < 5; $i++)
		{
			//books			
			$book = new Book("BOOK000001", "War and Peace", 20.0, 2.0);
			$book->store($db);					
			
			//DVDs
			$dvd = new DVD("DVD0000123", "Acme DVD-RW", 1.0, 700);
			$dvd->store($db);
			
			//Chairs
			$chair = new Furniture("TR12055555", "Chair", 40.0, 24, 45, 15);
			$chair->store($db);
		}
	}
}

?>