<?php
namespace SWApp\Forms;

use SWApp\Forms\FormProduct;

class FormFurniture extends FormProduct
{
	protected $name = 'furniture';
	protected $form = 'SWApp/Views/block_furniture.html';
	
	public function __construct()
	{		
		$this->fields['width'] = ['required','number'];
		$this->fields['height'] = ['required','number'];
		$this->fields['length'] = ['required','number'];
		
		parent::__construct();
	}
}

?>