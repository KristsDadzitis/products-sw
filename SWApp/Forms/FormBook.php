<?php
namespace SWApp\Forms;

use SWApp\Forms\FormProduct;

class FormBook extends FormProduct
{
	protected $name = 'book';
	protected $form = 'SWApp/Views/block_book.html';
	
	public function __construct()
	{		
		$this->fields['weight'] = ['required','number'];
		
		parent::__construct();
	}
}

?>