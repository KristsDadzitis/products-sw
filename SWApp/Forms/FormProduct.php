<?php
namespace SWApp\Forms;

class FormProduct
{
	protected $fields = ['sku' => ['required'],
						 'name' => ['required'],
						 'price' => ['required','number'],
						];
						
	protected $name = 'product';
	protected $form = 'SWApp/Views/block_product.html';
	protected $values = []; //values array for html display
	protected $errors = [];	//errors array for html display
	
	public function __construct()
	{
		foreach($this->fields as $field => $rules)
		{
			$this->errors[$field] = '';
			$this->values[$field] = '';
		}
	}
	
	public function validateData(array $data) : bool
	{
		$e = 0;
		
		foreach($this->fields as $field => $rules)
		{
			$this->errors[$field] = '';
			$this->values[$field] = '';
			
			if (isset($data[$field]))
				$this->values[$field] = $data[$field];
			
			foreach($rules as $r)
			{
				$required = false;
				
				switch($r)
				{
					case 'required':
						if (empty($data[$field]))
						{
							$this->errors[$field] = "Field is required.";
							$e++;
							$required = true;
							$this->values[$field] = '';
						}
						break;
						
					case 'number':
						if (!is_numeric($data[$field]))
						{
							$this->errors[$field] = "Field must be a number.";
							$e++;
							$this->values[$field] = '';
						}
						break;
				}
				
				if ($required) //don't check other rules if required field is missing
					break;
			}
		}
		
		if ($e > 0)
			return false;
		
		return true;
	}
	
	public function getErrors(): array
	{
		return $this->errors;
	}
	
	public function showForm()
	{
		$values = $this->values;
		$errors = $this->errors;
		
		include $this->form;
	}
	
	public function getName()
	{
		return $this->name;
	}
	
	public function setForm(string $form)
	{
		$this->form = $form;
	}
}
?>