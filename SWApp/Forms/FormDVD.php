<?php
namespace SWApp\Forms;

use SWApp\Forms\FormProduct;

class FormDVD extends FormProduct
{
	protected $name = 'dvd';
	protected $form = 'SWApp/Views/block_dvd.html';
	
	public function __construct()
	{		
		$this->fields['size'] = ['required','number'];
		
		parent::__construct();
	}
}

?>