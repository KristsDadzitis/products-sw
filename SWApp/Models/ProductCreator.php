<?php
namespace SWApp\Models;

use SWApp\Models\Product;
use SWApp\Models\Book;
use SWApp\Models\DVD;
use SWApp\Models\Furniture;

class ProductCreator
{
	protected string $type = "product";
	protected array $data = [];
	
	public function __construct(string $type, array $data)
	{	
		$this->type = $type;
		$this->data = $data;
	}
	
	public function create() : Product
	{
		$sku = isset($this->data['sku']) ? $this->data['sku'] : "";
		$name = isset($this->data['name']) ? $this->data['name'] : "";
		$price = isset($this->data['price']) ? $this->data['price'] : "";
		
		switch($this->type)
		{
			case 'book':
				$weight = isset($this->data['weight']) ? $this->data['weight'] : "";
				$book = new Book($sku, $name, $price, $weight);
				return $book;
			
			case 'dvd':
				$size = isset($this->data['size']) ? $this->data['size'] : "";
				$dvd = new DVD($sku, $name, $price, $size);
				return $dvd;
				
			case 'furniture':
				$height = isset($this->data['height']) ? $this->data['height'] : 0;
				$width = isset($this->data['width']) ? $this->data['width'] : 0;
				$length = isset($this->data['length']) ? $this->data['length'] : 0;
				$furniture = new Furniture($sku, $name, $price, $height, $width, $length);
				return $furniture;
			
			default: //create generic product if no type matches
				$product = new Product($sku, $name, $price);
				return $product;
		}
	}
}

?>