<?php
namespace SWApp\Models;

use SWApp\Models\Product;
use SWApp\Database\DatabaseInterface;

class Book extends Product
{	
	protected float $weight;
	
	public function __construct(string $sku, string $name, float $price, float $weight)
	{		
		$this->sku = $sku;
		$this->name = $name;
		$this->price = $price;
		$this->weight = $weight;
	}
	
	public function display()
	{
		$vars = [];
		$vars['sku'] = $this->sku;
		$vars['name'] = $this->name;
		$vars['price'] = '$ ' . number_format((float)$this->price, 2, '.', ' ');
		$vars['desc'] = "Weight: " . number_format((float)$this->weight, 2, '.', ' ') . " Kg";
		
		return $vars;
	}
	
	public function store(DatabaseInterface $db)
	{
		$values = array(
			'sku' => $this->sku,
			'name' => $this->name,
			'price' => $this->price,
			'data' => json_encode(array('weight'=> $this->weight)),
			'type' => 2,
		);
		
		$db->insert('products', $values);
	}
	
	public function showForm()
	{
		include 'SWApp/Views/block_book.html';
	}
}

?>