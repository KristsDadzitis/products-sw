<?php
namespace SWApp\Models;

use SWApp\Models\Product;
use SWApp\Database\DatabaseInterface;

class Furniture extends Product
{	
	protected float $height;
	protected float $width;
	protected float $length;	
	
	public function __construct(string $sku, string $name, float $price, float $height, float $width, float $length)
	{		
		$this->sku = $sku;
		$this->name = $name;
		$this->price = $price;
		$this->height = $height;
		$this->width = $width;
		$this->length = $length;
	}
	
	public function display()
	{
		$vars = [];
		$vars['sku'] = $this->sku;
		$vars['name'] = $this->name;
		$vars['price'] = '$ ' . number_format((float)$this->price, 2, '.', ' ');
		$vars['desc'] = "Dimensions: " . $this->height . "x" . $this->width . "x" . $this->length;
		
		return $vars;
	}
	
	public function store(DatabaseInterface $db)
	{
		$values = array(
			'sku' => $this->sku,
			'name' => $this->name,
			'price' => $this->price,
			'data' => json_encode(array(
				'height'=> $this->height,
				'width'=> $this->width,
				'length'=> $this->length,				
			)),
			'type' => 4,
		);
		
		$db->insert('products', $values);
	}
}

?>