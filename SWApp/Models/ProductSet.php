<?php
namespace SWApp\Models;

use SWApp\Database\DatabaseInterface;
use SWApp\Database\DBQuery;
use SWApp\Models\ProductCreator;

class ProductSet
{
	protected $products = [];
	protected $database = null;
	
	public function __construct(DatabaseInterface $db)
	{
		$this->database = $db;
	}
	
	public function fetch(int $limit = 0, string $orderBy = '', bool $asc = true) //fetch all if limit = 0
	{
		$query = new DBQuery();		
		$query = $query->select("products", "*")
					   ->leftJoin("types", "products.type=types.type_id")
					   ->orderBy($orderBy, $asc)
					   ->limit($limit);
					   
		$res = $this->database->fetchDBQuery($query);
		
		$this->products = [];
		
		foreach ($res as $p)
		{		
			$data = [];
			
			if (isset($p['data']))
			{
				$data = json_decode($p['data'], true);
				$p = array_merge($p, $data);
			}		
			
			$product = new ProductCreator($p['type_name'], $p);
			$this->products[$p['id']] = $product->create();
		}
	}
	
	public function getList():array
	{		
		$res = [];
		
		foreach($this->products as $k => $p)
		{
			$res[$k] =  $p->display(); //set product id as array key
		}
		
		return $res;
	}
}

?>