<?php
namespace SWApp\Models;

use SWApp\Database\DatabaseInterface;

class Product
{
	protected int $id;
	protected string $sku;
	protected string $name;
	protected float $price;
	
	public function __construct(string $sku, string $name, float $price)
	{
		$this->sku = $sku;
		$this->name = $name;
		$this->price = $price;
	}
	
	public function display()
	{
		$vars = [];
		$vars['sku'] = $this->sku;
		$vars['name'] = $this->name;
		$vars['price'] = '$ ' . number_format((float)$this->price, 2, '.', ' ');
		
		return $vars;
	}
	
	public function store(DatabaseInterface $db)
	{
		$values = array(
			'sku' => $this->sku,
			'name' => $this->name,
			'price' => $this->price,
		);
		
		$db->insert('products', $values);
	}
}

?>