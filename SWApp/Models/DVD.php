<?php
namespace SWApp\Models;

use SWApp\Models\Product;
use SWApp\Database\DatabaseInterface;

class DVD extends Product
{	
	protected float $size;
	
	public function __construct(string $sku, string $name, float $price, int $size)
	{		
		$this->sku = $sku;
		$this->name = $name;
		$this->price = $price;
		$this->size = $size;
	}
	
	public function display()
	{		
		$vars = [];
		$vars['sku'] = $this->sku;
		$vars['name'] = $this->name;
		$vars['price'] = '$ ' . number_format((float)$this->price, 2, '.', ' ');
		$vars['desc'] = "Size: " . number_format((float)$this->size, 0, '.', ' ') . " MB";
		
		return $vars;
	}
	
	public function store(DatabaseInterface $db)
	{
		$values = array(
			'sku' => $this->sku,
			'name' => $this->name,
			'price' => $this->price,
			'data' => json_encode(array('size'=> $this->size)),
			'type' => 3,
		);
		
		$db->insert('products', $values);
	}
}

?>