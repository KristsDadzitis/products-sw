<?php
namespace SWApp\Models;

use SWApp\Database\DatabaseInterface;
use SWApp\Database\DBQuery;

class CategorySet
{
	protected $categories = [];
	protected $database = null;
	
	public function __construct(DatabaseInterface $db)
	{
		$this->database = $db;
	}
	
	public function fetchAll(): array
	{
		$query = new DBQuery();
		$query = $query->select('types', '*')
					   ->orderBy('type_id', false);
					   
		return $this->database->fetchDBQuery($query);
	}
}

?>