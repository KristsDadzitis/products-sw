<?php
	
	include 'SWApp/Misc/autoload.php';
	
	use SWApp\Database\MySqlDB;
	use SWApp\Page;
	
	$db = new MySqlDB;
	$db->connect();
	
	$page = new Page();
	$page->createSampleData($db); //creates tables for categories and products with sample product data
	$page->goToPage("list");
?>