function loadForm(type)
{
	$("#formContainer").load('load_form_' + type + '.php', function() {
		//on load complete
	});
}

$(document).ready(function(){
	$("#ddType").change(function(){
		var selected = $(this).children("option:selected").val();

		loadForm(selected);	
	});
	
	for (i = 0; i < 3; i++)
		$("#saved").fadeOut(150).fadeIn(150);
});

