var deleteList = [];

$("[name=cbProduct]").each(function() {
	$(this).on("change", function() {
		deleteList = [];
		$("[name=cbProduct]").each(function()
		{
			if ($(this).prop("checked"))
				deleteList.push($(this).val());
			
			$("#selectedItems").prop("value", deleteList);
		});
	});
});

$(".dropdown-item").click(function() {
	var action = $(this).prop("id");
	var text = $(this).text();
	
	$("#action").prop("value", action);
	$("#ddAction").text(text);
});
