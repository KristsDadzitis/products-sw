<?php
	namespace SWApp;

	include 'SWApp/Misc/autoload.php';
	
	use SWApp\Database\MySqlDB;
	use SWApp\Models\ProductSet;
	use SWApp\Page;
	
	$list = []; //array for storing fetched products
	
	$db = new MySqlDB;
	$db->connect();
	
	if (isset($_POST['action'])) //process actions from the page
	{
		switch ($_POST['action'])
		{
			case "addItem":
				$addItem = new Page();
				$addItem->goToPage('new');
				break;
				
			case "massDelete":	
				if (isset($_POST['selectedItems']))
					$db->delete('products', 'id', $_POST['selectedItems']);
				break;			
		}
	}	

	$products = new ProductSet($db);
	$products->fetch(0, 'type'); //fetch all products and sort by type
	
	$list = $products->getList(); //get products with ids as array keys
	
	include 'SWApp/Views/list.html';

?>