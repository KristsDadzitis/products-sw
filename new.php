<?php
namespace SWApp;

	include 'SWApp/Misc/autoload.php';
	
	use SWApp\Database\MySqlDB;
	use SWApp\Models\CategorySet;
	use SWApp\Models\ProductCreator;
	use SWApp\Forms\FormProduct;
	use SWApp\Forms\FormBook;
	use SWApp\Forms\FormDVD;
	use SWApp\Forms\FormFurniture;
	
	$db = new MySqlDB;
	$db->connect();
	
	$form = new FormProduct; //form for validating and showing errors
	
	$type_required = ""; //message if type not selected
	$product_saved = ""; //message when new product saved
	$type_selected = ""; //for remembering what type was selected
	$types = [];		//array for categories to display in dropbox
	
	if (!empty($_POST)) //check saved form
	{
		if(isset($_POST['type'])) //type must be set
		{
			switch($_POST['type']) //check type for validation
			{
				case 'book':
					$form = new FormBook;
					break;
					
				case 'dvd':
					$form = new FormDVD;
					break;
					
				case 'furniture':
					$form = new FormFurniture;
					break;
					
				case 'product':
					$form = new FormProduct;
					break;
			}
			
			if ($form->validateData($_POST)) //if valid then save
			{
				$product = new ProductCreator($_POST['type'], $_POST);
				$product->create()->store($db);
				
				$product_saved = "Product Saved!"; //set message for feedback
			}
			
			$type_selected = $_POST['type']; //remember type for better UX
		}
		else
		{
			$type_required = "Product type must be selected."; //if type not selected, show reminder
		}
	}
	
	$categories = new CategorySet($db);
	$types = $categories->fetchAll();
	
	include 'SWApp/Views/new.html';

?>